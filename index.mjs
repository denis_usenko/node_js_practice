import * as utils from 'test-utils';
import util from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

const runMePleasePromise = util.promisify(utils.runMePlease);

try {
    const result = await runMePleasePromise(params);
    console.log(result);
} catch (error) {
    console.log(error);
}